import React from 'react';
import SectionBanner from './section-banner';

const Projects = () => {
  return (
    <div className="projects">
      <SectionBanner id={"section-banner_projects"} sectionName={"Projects"}/>
      <div className="projects_wrap">
        {/* <div className="projects_item">
          <a href="https://samuelts.com/PomodoroTimer" target="_blank" id="pomodoro-timer">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/PomodoroTimer.png" alt="pomodoro timer"/>
            <h3 className="projects_item_section">Pomodoro Timer</h3>
          </a>
          <a href="https://gitlab.com/samuelts/PomodoroTimer" className="projects_item_hover" target="_blank" id="pomodoro-timer-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">React</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/webpack.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Webpack</div>
            </div>
          </a>
        </div> */}

        <div className="projects_item">
          <a href="https://samuelts.com/ReactCalc" target="_blank" id="react-calc">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/Calculator.png" alt="react calc"/>
            <h3 className="projects_item_section">Calculator</h3>
          </a>
          <a href="https://gitlab.com/samuelts/ReactCalc" className="projects_item_hover" target="_blank" id="react-calc-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">React</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/webpack.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Webpack</div>
            </div>
          </a>
        </div>

        <div className="projects_item">
          <a href="https://samuelts.com/ReactDrumMachine" target="_blank" id="react-drum-machine">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/DrumMachine.png" alt="react drum machine"/>
            <h3 className="projects_item_section">Drum Machine</h3>
          </a>
          <a href="https://gitlab.com/samuelts/ReactDrumMachine" className="projects_item_hover" target="_blank" id="react-drum-machine-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">React</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/webpack.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Webpack</div>
            </div>
          </a>
        </div>

        <div className="projects_item">
          <a href="https://samuelts.com/ReduxBlog" target="_blank" id="redux-blog">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/ReduxBlog.png" alt="redux blog"/>
            <h3 className="projects_item_section">Blog</h3>
          </a>
          <a href="https://gitlab.com/samuelts/ReduxBlog" className="projects_item_hover" target="_blank" id="redux-blog-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">React</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/redux.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Redux</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/bootstrap.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Bootstrap</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/webpack.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Webpack</div>
            </div>
          </a>
        </div>

        <div className="projects_item">
          <a href="https://samuelts.com/ReactMarkdownEditor" target="_blank" id="react-markdown-editor">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/md_editor.png" alt="react/redux markdown editor"/>
            <h3 className="projects_item_section">Markdown Editor</h3>
          </a>
          <a href="https://gitlab.com/samuelts/ReactMarkdownEditor" className="projects_item_hover" target="_blank" id="react-markdown-editor-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">React</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/redux.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Redux</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/bootstrap.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Bootstrap</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/webpack.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Webpack</div>
            </div>
          </a>
        </div>

        <div className="projects_item">
          <a href="https://samuelts.com/ReduxWeatherApp" target="_blank" id="redux-weather-app">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/redux_weather_app.png" alt="react/redux weather app"/>
            <h3 className="projects_item_section">Weather App</h3>
          </a>
          <a href="https://gitlab.com/samuelts/ReduxWeatherApp" className="projects_item_hover" target="_blank" id="redux-weather-app-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">React</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/redux.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Redux</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/bootstrap.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Bootstrap</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/webpack.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Webpack</div>
            </div>
          </a>
        </div>

        <div className="projects_item">
          <a href="https://samuelts.com/ReactYoutubeSearch" target="_blank" id="react-youtube-search">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/reactjs_youtube_player_img.png" alt="react youtube search app"/>
            <h3 className="projects_item_section">Youtube Search App</h3>
          </a>
          <a href="https://gitlab.com/samuelts/ReactYoutubeSearch" className="projects_item_hover" target="_blank" id="react-youtube-search-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">React</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/redux.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Redux</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/bootstrap.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Bootstrap</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/webpack.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">Webpack</div>
            </div>
          </a>
        </div>

        <div className="projects_item">
          <a href="https://samuelts.com/quoter" target="_blank" id="quoter">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/quoter_img.png" alt="quote machine"/>
            <h3 className="projects_item_section">Quote Machine</h3>
          </a>
          <a href="https://gitlab.com/samuelts/quoter" className="projects_item_hover" target="_blank" id="quoter-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/html5.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">HTML</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/css3.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">CSS</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/javascript.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">JS</div>
            </div>
          </a>
        </div>

        <div className="projects_item">
          <a href="https://codepen.io/safwyl/full/KxjPEN/" target="_blank" id="pig-latin">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/piglatin_cap.png" alt="pig latin translator"/>
            <h3 className="projects_item_section">Pig Latin Translator</h3>
          </a>
          <a href="https://codepen.io/safwyl/pen/KxjPEN" className="projects_item_hover" target="_blank" id="pig-latin-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/html5.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">HTML</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/css3.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">CSS</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/javascript.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">JS</div>
            </div>
          </a>
        </div>

        <div className="projects_item">
          <a href="https://codepen.io/safwyl/full/BOxGRR" target="_blank" id="techdoc">
            <img className="projects_item_img" src="https://gitlab.com/samuelts/logos/raw/master/techdocs_cap.png" alt="technical documentation"/>
            <h3 className="projects_item_section">Technical Documentation</h3>
          </a>
          <a href="https://codepen.io/safwyl/pen/BOxGRR" className="projects_item_hover" target="_blank" id="redux-blog-source">
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/html5.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">HTML</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/css3.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">CSS</div>
            </div>
            <div>
              <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/javascript.svg" className="projects_item_hover_icons"/>
              <div className="projects_item_hover_labels">JS</div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}
export default Projects;