import React from 'react';

const Header = (props) => {
  return (
    <div className="header" style={props.style}>
      <div className="header_links">
        <button
          className="header_links_about"
          id={props.currSection === "About" ? "activeLink" : null}
          onClick={props.onClickAbout}
        >
          About Me
        </button>
        -
        <button
          className="header_links_projects"
          id={props.currSection === "Projects" ? "activeLink" : null}
          onClick={props.onClickProjects}
        >
          Projects
        </button>
        -
        <button
          className="header_links_contact"
          id={props.currSection === "Contact" ? "activeLink" : null}
          onClick={props.onClickContact}
        >
          Contact Me
        </button>
      </div>
    </div>
  );
}
export default Header;