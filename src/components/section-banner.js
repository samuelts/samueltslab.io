import React from 'react';

const SectionBanner = (props) => {
  return (
    <div className="section-banner" id={props.id}>
      {props.sectionName}
    </div>
  );
}

export default SectionBanner;