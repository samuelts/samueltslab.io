import React from 'react';
import SectionBanner from './section-banner';

const Contact = () => {
  return (
    <div className="contact">
    <SectionBanner id={"section-banner_contact"} sectionName={"Contact Me"}/>
      <div className="contact_wrap">
        <div className="contact_row">
          <a href="https://www.linkedin.com/in/stephsam/" target="_blank">
            <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/linkedin.svg" className="contact_icons"/>
            <div className="contact_labels">LinkedIn</div>
          </a>
          <a id="profile-link" href="https://github.com/safwyls" target="_blank">
            <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/github.svg" className="contact_icons"/>
            <div className="contact_labels">GitHub</div>
          </a>
          <a href="https://www.freecodecamp.org/safwyl" target="_blank">
            <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/freecodecamp.svg" className="contact_icons"/>
            <div className="contact_labels">FreeCodeCamp</div>
          </a>
          <a href="https://codepen.io/safwyl/" target="_blank">
            <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/codepen.svg" className="contact_icons"/>
            <div className="contact_labels">Codepen</div>
          </a>
          <a href="mailto:samueltstephenson@gmail.com">
            <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/gmail.svg" className="contact_icons"/>
            <div className="contact_labels">Gmail</div>
          </a>
        </div>
      </div>
    </div>
  );
}
export default Contact;