import React, { Component } from 'react';
import Particles from 'react-particles-js';
import detectIt from 'detect-it';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

library.add(faChevronDown);

class Intro extends Component {
  constructor(props) {
    super(props);

    this.state = {
      params: {}
    };
  }

  shouldComponentUpdate() {
    return false;
  }

  componentDidMount() {
    const pixelRatio = window.devicePixelRatio;
    const isTouch = detectIt.primaryInput === 'touch' ? true : false;
    this.setState({
      params: {
        "particles": {
          "number": {
            "value": pixelRatio > 1 ? 10 : 50,
            "density": {
              "enable": true,
              "value_area": 800
            }
          },
          "color": {
            "value": "#50fa7b"
          },
          "shape": {
            "type": "circle",
            "stroke": {
              "width": 0,
              "color": "#000000"
            },
            "polygon": {
              "nb_sides": 5
            },
            "image": {
              "src": "img/github.svg",
              "width": 100,
              "height": 100
            }
          },
          "opacity": {
            "value": 0.5,
            "random": false,
            "anim": {
              "enable": false,
              "speed": 1,
              "opacity_min": 0.1,
              "sync": false
            }
          },
          "size": {
            "value": 3,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 40,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": true,
            "distance": 150,
            "color": "#8be9fd",
            "opacity": 0.4,
            "width": 1
          },
          "move": {
            "enable": true,
            "speed": 6,
            "direction": "none",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
              "enable": false,
              "rotateX": 600,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": isTouch ? false : true,
              "mode": "bubble"
            },
            "onclick": {
              "enable": true,
              "mode": isTouch ? "repulse" : "bubble"
            },
            "resize": true
          },
          "modes": {
            "grab": {
              "distance": 100,
              "line_linked": {
                "opacity": 1
              }
            },
            "bubble": {
              "distance": 400,
              "size": 5,
              "duration": 2,
              "opacity": 1,
              "speed": 3
            },
            "repulse": {
              "distance": 100,
              "duration": 0.4
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true
      }
    });
  }

  render() {

    return (
      <div className="intro">
        <Particles
          params={this.state.params}
          width="100%"
          height="100%"
          className="intro_particles"
          canvasClassName="intro_particles_canvas"
          style={{
            position: "absolute",
            top: "0",
            left: "0",
            overflow: "hidden"
            }}
        />
        <div className="intro_info">
          <div className="intro_info_name">Samuel Stephenson</div>
          <div className="intro_info_subtitle">Web Dev, Designer, Engineer</div>
        </div>
        <button className="intro_scroll" onClick={this.props.onClickAbout}>
          <FontAwesomeIcon className="intro_scroll_arrow" icon="chevron-down"/>
          <div className="intro_scroll_text">Scroll</div>
        </button>
      </div>
    );
  }
}
export default Intro;