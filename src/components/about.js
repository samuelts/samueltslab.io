import React from 'react';
import SectionBanner from './section-banner';

const About = () => {
  return (
    <div className="about">
    <SectionBanner id={"section-banner_about"} sectionName={"About Me"}/>
    <div className="about_wrap">
      <div className="about_intro">
        Hi! My name is Sam and I'm a web developer.
        <br/>
        <br/>
        I am a graduate of Oregon State University with a Bachelor of Science in Physics with strong
        interests in technology development, computer science, web development, and programming.
        <br/>
        <br/>
        Currently I work at Intel in Hillsboro, Oregon as a full-stack software engineer focusing on 
        front-end development for web and desktop applications.
      </div>
      <div className="about_divider">
          <i className="fas fa-minus"></i>
      </div>
      <div className="about_techs-intro">
        Here are some of the technologies I work with.
      </div>
      <div className="about_techs">
        <div className="about_row">
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/html5.svg" className="about_icons"/>
          <div className="about_labels">HTML5</div>
        </a>
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/css3.svg" className="about_icons"/>
          <div className="about_labels">CSS3</div>
        </a>
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/javascript.svg" className="about_icons"/>
          <div className="about_labels">JS</div>
        </a>
        </div>
        <div className="about_row">
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" className="about_icons"/>
          <div className="about_labels">React</div>
        </a>
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/redux.svg" className="about_icons"/>
          <div className="about_labels">Redux</div>
        </a>
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/bootstrap.svg" className="about_icons"/>
          <div className="about_labels">Bootstrap</div>
        </a>
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/jquery.svg" className="about_icons"/>
          <div className="about_labels">jQuery</div>
        </a>
        </div>
        <div className="about_row">
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/nodejs.svg" className="about_icons"/>
          <div className="about_labels">node.js</div>
        </a>
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/python.svg" className="about_icons"/>
          <div className="about_labels">Python</div>
        </a>
        <a href="https://gitlab.com/samuelts/projects" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/dot-net.svg" className="about_icons"/>
          <div className="about_labels">.NET</div>
        </a>
        </div>
      </div>
      </div>
    </div>
  );
}
export default About;