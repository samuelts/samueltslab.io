import React, { Component } from 'react';
import Header from './header';
import Intro from './intro';
import About from './about';
import Projects from './projects';
import Contact from './contact';
import ScrollToTop from 'react-scroll-up';

export default class App extends Component {
  constructor (props) {
    super(props);

    this.state = {
      headerHeight: 0,
      aboutBannerOffset: 0,
      projectsBannerOffset: 0,
      contactBannerOffset: 0,
      currSection: null
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.onClickAbout = this.onClickAbout.bind(this);
    this.onClickProjects = this.onClickProjects.bind(this);
    this.onClickContact = this.onClickContact.bind(this);
  }

  componentDidMount() {
    this.setState({
      headerHeight: document.getElementsByClassName("header")[0].offsetHeight * 2 / 5 ,
      aboutBannerOffset: document.getElementById("section-banner_about").offsetTop,
      projectsBannerOffset: document.getElementById("section-banner_projects").offsetTop,
      contactBannerOffset: document.getElementById("section-banner_contact").offsetTop,
    });
    
    window.addEventListener('scroll', this.handleScroll, true);
  }

  handleScroll() {
    const aboutBannerOffset = this.state.aboutBannerOffset
    const projectsBannerOffset = this.state.projectsBannerOffset
    const contactBannerOffset = this.state.contactBannerOffset
    const headerHeight = this.state.headerHeight;
    if (window.pageYOffset >= aboutBannerOffset - headerHeight && window.pageYOffset < projectsBannerOffset - headerHeight) {
      this.setState({
        currSection: "About"
      });
    } else if (window.pageYOffset >= projectsBannerOffset- headerHeight && window.pageYOffset < contactBannerOffset - headerHeight) {
      this.setState({
        currSection: "Projects"
      });
    } else if (window.pageYOffset >= contactBannerOffset - headerHeight) {
      this.setState({
        currSection: "Contact"
      });
    } else {
      this.setState({
        currSection: null
      });
    }
  }

  onClickAbout() {
    document.querySelector('.about').scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  onClickProjects() {
    document.querySelector('.projects').scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  onClickContact() {
    document.querySelector('.contact').scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  render() {
    return (
      <div className='container'>
        <Header
          onClickAbout={this.onClickAbout}
          onClickProjects={this.onClickProjects}
          onClickContact={this.onClickContact}
          currSection={this.state.currSection}
        />
        <div className='content'>
          <Intro onClickAbout={this.onClickAbout}/>
          <About />
          <Projects />
          <Contact />
          <ScrollToTop showUnder={160}
            style={{bottom: '2em', right: '2em'}}>
            <span className="scroll-to-top"><i className="fas fa-chevron-up"></i></span>
          </ScrollToTop>
        </div>
      </div>
    );
  }
}
