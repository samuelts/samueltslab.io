import React from 'react'

import SEO from '../components/seo'
import App from '../components/app'

const IndexPage = () => (
  <div>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <App/>
  </div>
)

export default IndexPage
